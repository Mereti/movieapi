package com.demo.springboot;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class MoviesService {

    private final MovieListDto movies;

    public MoviesService() {
        List<MovieDto> moviesList = new ArrayList<>();
        moviesList.add(new MovieDto(1,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
        movies = new MovieListDto(moviesList);
    }


    public void addMovie(int id, String title, String image, int year) {
        MovieDto movieDto = new MovieDto(id,title,year,image);
        List<MovieDto> listDto = movies.getMovies();
        listDto.add(movieDto);
        movies.setMovies(listDto);

    }

    public boolean exist(int id) {
        return movies.getMovies().stream().anyMatch(movieDto -> movieDto.getMovieId().equals(id));
    }

    public void editMovie(int id, CreateMovieDto createMovieDto) {
        MovieDto mDto = movies.getMovies().stream().filter(movieDto -> movieDto.getMovieId().equals(id)).findAny().get();

        mDto.setImage(createMovieDto.getImage());
        mDto.setTitle(createMovieDto.getTitle());
        mDto.setYear(createMovieDto.getYear());
        mDto.setMovieId(createMovieDto.getMovieId());
    }

    public boolean deleteMovieById(int id) {
        return movies.getMovies().removeIf(movieDto -> movieDto.getMovieId().equals(id));
    }

    public MovieListDto getAllMovies() {
    List<MovieDto> listDto = movies.getMovies();
    listDto.sort((MovieDto o1,MovieDto o2) -> o2.getMovieId().compareTo(o1.getMovieId()));

    return movies ;
    }

}
