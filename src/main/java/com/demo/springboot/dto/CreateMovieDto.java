package com.demo.springboot.dto;

public class CreateMovieDto{
    private Integer movieId;
    private String title;
    private Integer year;
    private String image;

    public CreateMovieDto() {

    }
    public CreateMovieDto(Integer id,String title,Integer year,String image){
        this.movieId = id;
        this.title = title;
        this.year = year;
        this.image = image;

    }

    public Integer getMovieId() {return movieId;}

    public String getTitle() {
        return title;
    }
    public Integer getYear() {
            return year;
    }
    public String getImage() { return image; }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
