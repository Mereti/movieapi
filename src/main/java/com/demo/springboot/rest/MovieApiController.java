package com.demo.springboot.rest;

import com.demo.springboot.MoviesService;
import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api")
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    private MoviesService moviesService;

    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        LOG.info("--- get all movies: {}", moviesService.getAllMovies());
        return/* ResponseEntity.ok().body(movies); */    new ResponseEntity<>(moviesService.getAllMovies(), HttpStatus.OK);
    }

    @DeleteMapping("/movies/{id}")
    public ResponseEntity<Void> getMovie(@PathVariable("id") Integer id) {
        LOG.info("--- id: {}", id);
        if(moviesService.deleteMovieById(id)) {//stream -> z listy el strimien czy jest w nim jkikolwiek pasujacy element
            return ResponseEntity.ok().build();
        } else return ResponseEntity.notFound().build();

    }

    @PutMapping("/movies/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable("id") Integer id, @RequestBody CreateMovieDto createMovieDto) {
        LOG.info("--- id: {}", id);

        if(moviesService.exist(id)){
            moviesService.editMovie(id, createMovieDto);
            return ResponseEntity.ok().build();
        }else return ResponseEntity.notFound().build();
    }

    @PostMapping("/movies")
    public ResponseEntity<Void> createMovie(@RequestBody CreateMovieDto createMovieDto) throws URISyntaxException {
        LOG.info("--- id: {}", createMovieDto.getMovieId());
        LOG.info("--- title: {}", createMovieDto.getTitle());
        LOG.info("--- year: {}", createMovieDto.getYear());
        LOG.info("--- image: {}", createMovieDto.getImage());

        if(createMovieDto.getImage() == null || createMovieDto.getTitle() == null || createMovieDto.getMovieId() == null || createMovieDto.getYear() == null || createMovieDto.getYear() < 1895){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }else{
           moviesService.addMovie(createMovieDto.getMovieId(), createMovieDto.getTitle(), createMovieDto.getImage(), createMovieDto.getYear());

            return ResponseEntity.created(new URI("/movies" + createMovieDto.getMovieId())).build();
        }


    }
}
